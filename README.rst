===========================
Cookiecutter Python Package
===========================

Cookiecutter_ template for an internal Python package.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter


Quickstart
==========

Install the latest Cookiecutter if you haven't installed it yet ::

    conda install cookiecutter

Generate a Python package project::

    cookiecutter git@gitlab.com:ownconda/cookiecutter-python.git


Prompts
=======

When you create a package, you are prompted to enter these values:

Templated Values
----------------

The following appear in various parts of your generated project.

project_title
  The name of your new Python package project.  This is used in documentation,
  so spaces and any characters are fine here.  Example: *Spam Eggs*

project_name
  The project and repository name as it appears in GitLab.  May only contain
  small letters, numbers and "-" (``[a-z0-9-]``).  Example: *spam-eggs*

gitlab_group
  The GitLab group that your project belongs to.  Example: *apps*

package_name
  The Python package name.  This is usually the project name prefixed with
  *own_* and with "-" replaced with "_".  Example: *own_spam_eggs*

conda_name
  The Conda package name.  This is usually the project name prefixed with
  *own-*.  Example: *own-spam-eggs*

project_summary
  A short description of your project.  It appears on GitLab, in the README and
  in the Conda recipe.  Example:  *Project for producing only the finest Spam
  and Eggs.*


Options
-------

The following package configuration options set up different features for your
project.  Use **y** to enable them and **n** to disable them.

create_gitlab_project
  Whether you want Cookiecutter to also create a new GitLab project

has_click
  Whether you want to create a command line interface using Click_

has_custom_db
  Whether this project has a custom DB.  If yes, Alembic is
  initialized for this project.

extra_requirements
  A comma separated list with extra run requirements for your project.

.. _Click: http://click.pocoo.org/
