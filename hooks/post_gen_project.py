import pathlib
import shutil
import subprocess


PROJECT_DIRECTORY = pathlib.Path.cwd()


def remove_file(filepath):
    PROJECT_DIRECTORY.joinpath(filepath).unlink()


def remove_dir(path):
    shutil.rmtree(PROJECT_DIRECTORY / path)


def create_gitlab_project(group, name, desc):
    import gitlab

    print('Creating GitLab project ...')
    gl = gitlab.Gitlab.from_config('own')
    gl.auth()

    namespace_id = gl.groups.get(id=group).id

    try:
        project = gl.projects.get(f'{group}/{name}')
        print('project already exists')
    except gitlab.GitlabGetError:
        project = gl.projects.create({
            'name': name,
            'namespace_id': namespace_id,
            'description': desc,
            'visibility': 'private',
            'issues_enabled': True,
            'merge_requests_enabled': True,
            'jobs_enabled': True,
            'wiki_enabled': False,
            'snippets_enabled': False,
            'lfs_enabled': True,
            'printing_merge_request_link_enabled': True,
            'public_jobs': False,
        })

    return project.ssh_url_to_repo


def main():
    pathlib.Path('conda/meta.yaml.jinja2').rename('conda/meta.yaml')

    if '{{ cookiecutter.has_click }}' == 'n':
        remove_file('src/{{ cookiecutter.package_name }}/__main__.py')
        remove_file('tests/test_main.py')

    if '{{ cookiecutter.has_docs }}' == 'n':
        remove_dir('docs')

    if '{{ cookiecutter.has_custom_db }}' == 'n':
        remove_dir('alembic')
        remove_file('alembic.ini')
        remove_file('src/{{ cookiecutter.package_name }}/model.py')
        remove_file('tests/test_model.py')
        remove_file('with_docker.sh')

    group = '{{ cookiecutter.gitlab_group }}'
    name = '{{ cookiecutter.project_name }}'
    desc = '{{ cookiecutter.project_summary.replace("'", "\\'") }}'
    ssh_url_to_repo = f'git@gitlab.com:{group}/{name}.git'
    if '{{ cookiecutter.create_gitlab_project }}' == 'y':
        url = create_gitlab_project(group, name, desc)
        assert url == ssh_url_to_repo

    subprocess.run(['git', 'init'], cwd=PROJECT_DIRECTORY)
    subprocess.run(['git', 'remote', 'add', 'origin', ssh_url_to_repo], cwd=PROJECT_DIRECTORY)


if __name__ == '__main__':
    main()
