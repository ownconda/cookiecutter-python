import re
import sys


NAME_REGEX = r'^[a-z0-9][a-z0-9-]*$'
PROJECT_NAME = '{{ cookiecutter.project_name }}'


if not re.match(NAME_REGEX, PROJECT_NAME):
    print(f'ERROR: The project name "{PROJECT_NAME}" is not a valid.  '
          f'It must only contain small letters, numbers and dashes and must '
          f'not begin with a dash.')

    # Exit to cancel project
    sys.exit(1)
