"""
Init

Revision ID: 0001
Revises:
Create Date: {% now 'local', '%Y-%m-%d %H:%M:%S' %}

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0001'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        '{{ cookiecutter.project_name }}_data',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('name', sa.String(length=50), unique=True, nullable=False,),
        mysql_character_set='utf8mb4',
        mysql_collate='utf8mb4_unicode_520_ci',
    )


def downgrade():
    op.drop_table('{{ cookiecutter.project_name}}_data')
