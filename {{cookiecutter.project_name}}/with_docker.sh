#!/bin/bash
#
# Spawn the specified Docker container if it is not already running and
# execute the arguments string as shell script.
#
# Usage:
#   ./with_docker.py pytest
#
IMAGE='mariadb:latest'
CONTAINER='mariadb'

export MYSQL_HOST='127.0.0.1'
export MYSQL_PORT='3307'
export MYSQL_DATABASE='test'
export MYSQL_ROOT_PASSWORD='rootmysql'
export DB_URL="mysql://root:${MYSQL_ROOT_PASSWORD}@${MYSQL_HOST}:${MYSQL_PORT}/${MYSQL_DATABASE}"

if [[ ! $(docker ps | grep $CONTAINER) ]]; then
    docker run --detach --rm \
        --env "MYSQL_DATABASE=$MYSQL_DATABASE" \
        --env "MYSQL_ROOT_PASSWORD=$MYSQL_ROOT_PASSWORD" \
        --name $CONTAINER \
        --publish $MYSQL_PORT:3306 \
        $IMAGE --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_520_ci
    echo -n 'Waiting for server '
    until python -c "import MySQLdb; MySQLdb.connect(host='$MYSQL_HOST', port=$MYSQL_PORT, user='root', password='$MYSQL_ROOT_PASSWORD', db='$MYSQL_DATABASE')" &> /dev/null; do
        echo -n '.'
        sleep 1
    done
    echo ' done'
    echo ''
fi

# Run the arguments string as shell script:
$@

echo ''
echo "Run 'docker stop $CONTAINER' to stop the MariaDB Docker container."
echo ''
