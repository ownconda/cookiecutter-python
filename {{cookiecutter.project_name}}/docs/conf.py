import os
import datetime


# -- General configuration ------------------------------------------------

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.doctest',
    'sphinx.ext.intersphinx',
    'sphinx.ext.viewcode',
]

source_suffix = '.rst'
master_doc = 'index'
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']
rst_prolog = """
.. role:: channel(strong)
.. role:: branch(file)
.. role:: cmd(literal)
"""

templates_path = ['_templates']
pygments_style = 'sphinx'

project = '{{ cookiecutter.project_title }}'
slug = '{{ cookiecutter.project_name }}'
author = 'Own Company'
today_fmt = '%Y-%m-%d'
today = f'{datetime.date.today():{today_fmt}}'
version = os.environ.get('SPHINX_VERSION', '')
release = 'v{} ({}) – status: internal'.format(
    os.environ.get('SPHINX_RELEASE', ''), os.getenv("GIT_DESCRIBE_HASH", ''),
)
copyright = f'{% now 'utc', '%Y' %} {author} – {release} – {today}'

language = 'en'


# -- Options for HTML output ----------------------------------------------

import sphinx_rtd_theme
html_static_path = ['_static']
html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]
html_theme = 'sphinx_rtd_theme'
html_theme_options = {
    'display_version': False,
}


# -- Options for LaTeX output ---------------------------------------------

latex_engine = 'lualatex'
latex_elements = {
    'papersize': 'a4paper',
    'pointsize': '11pt',
    'preamble': '',
    'figure_align': 'htbp',
}

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
latex_documents = [
    (
        master_doc,
        f'{slug}.tex',
        project,
        author.replace('&', '\\&'),
        'manual',
    ),
]


# -- Plug-ins -------------------------------------------------------------

# Intersphinx
intersphinx_mapping = {
    'python': ('https://docs.python.org/3/', None),
{%- if cookiecutter.has_click == 'n' %}
    # 'click': ('https://click.palletsprojects.com/en/7.x/', None),
{%- else %}
    'click': ('https://click.palletsprojects.com/en/7.x/', None),
{%- endif %}
    # 'numpy': ('https://docs.scipy.org/doc/numpy/', None),
    # 'scipy': ('https://docs.scipy.org/doc/scipy/reference/', None),
}

# Autodoc
autodoc_member_order = 'bysource'
