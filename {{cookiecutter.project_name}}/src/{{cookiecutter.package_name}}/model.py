"""
{{ cookiecutter.project_title }} data(base) model.

"""
import os

from sqlalchemy import (
    Column,
    Integer,
    MetaData,
    String,
    Table,
    create_engine,
    delete,
    insert,
    select,
    update,
)
from sqlalchemy.engine.url import make_url
from sqlalchemy.exc import StatementError
import attr


__all__ = [
    'get_data', 'add_data', 'update_data', 'delete_data',
    'Data', 'StatementError',
]


TABLE_KWARGS = {
    'mysql_character_set': 'utf8mb4',
    'mysql_collate': 'utf8mb4_unicode_520_ci',
}
METADATA = MetaData(naming_convention={
    'ix': 'ix_%(column_0_label)s',
    'uq': 'uq_%(table_name)s_%(column_0_name)s',
    'ck': 'ck_%(table_name)s_%(constraint_name)s',
    'fk': 'fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s',
    'pk': 'pk_%(table_name)s'
})


def get_engine():
    """Return a :cls:`sqlalchemy.engine.Engine` with the url of the environment
    variable :envvar:`DB_URL`.

    """
    try:
        url = make_url(os.environ['DB_URL'])
    except KeyError:
        raise RuntimeError('Environment variable DB_URL is not set.') from None

    if url.drivername == 'mysql':
        url.query['charset'] = TABLE_KWARGS['mysql_character_set']
    engine = create_engine(url, encoding='utf-8', echo=False)
    return engine


def table(table_name):
    def decorate(cls):
        cls = attr.s(frozen=True)(cls)
        cls.T = Table(
            table_name,
            METADATA,
            *_columns_from_attrs(cls),
            **TABLE_KWARGS,
        )
        return cls
    return decorate


def column(sql_type, nullable=False, **kwargs):
    kwargs.update(nullable=nullable)
    attr_kws = {
        'metadata': {
            'sql_type': sql_type,
            'sql_kws': kwargs,
        },
    }
    if 'default' in kwargs:
        attr_kws['default'] = kwargs['default']
    return attr.ib(**attr_kws)


def _columns_from_attrs(cls):
    cols = []
    for field in attr.fields(cls):
        m = field.metadata
        cols.append(Column(field.name, m['sql_type'], **m['sql_kws']))
    return cols


@table('{{ cookiecutter.project_name }}_data')
class Data:
    """Represents {{ cookiecutter.project_title }} data."""
    id = column(Integer(), primary_key=True)
    name = column(String(length=50), unique=True)

    def __contains__(self, term):
        """Return ``True`` if one of the following attributes contains *term*:

        - :attr:`name`

        """
        term = term.lower()
        return term in self.name.lower()

    def evolve(self, **changes):
        """Create a copy of this data with *changes* applied."""
        return attr.evolve(self, **changes)


def get_data():
    """Return a list of all data (instances of :class:`Data`).

    """
    conn = get_engine().connect()
    stmt = select([Data.T]).order_by(Data.T.c.name)
    result = conn.execute(stmt)
    return [Data(**row) for row in result]


def add_data(data):
    """Add *data* to the database.

    Return a copy of *data* with :attr:`~Data.id` set to the actual ID.

    Raise a :exc:`sqlalchemy.exc.StatementError` if the data cannot be saved
    (e.g., because its name is already being used).

    """
    with get_engine().begin() as conn:
        ins = insert(Data.T).values(**attr.asdict(data))
        result = conn.execute(ins)
        data = attr.evolve(data, id=result.inserted_primary_key[0])
        return data


def update_data(base_data, edited_data):
    """Update *data* in the database.

    Raise a :exc:`sqlalchemy.exc.StatementError` if:

    - a data with ``data.id`` does not exist in the DB
    - a data with the same ``name`` already exists
    - that data has been modified by someone else.

    """
    if base_data.id != edited_data.id:
        raise ValueError(
            f'"base_data" and "edited_data" must have the same id: '
            f'{base_data.id} != {edited_data.id}'
        )

    with get_engine().begin() as conn:
        stmt = select([Data.T])\
            .with_for_update()\
            .where(Data.T.c.id == base_data.id)
        db_data = Data(**conn.execute(stmt).fetchone())

        if db_data != base_data:
            raise ValueError('Data has been modified on the server')

        stmt = update(Data.T)\
            .where(Data.T.c.id == edited_data.id)\
            .values(**attr.asdict(edited_data))
        conn.execute(stmt)


def delete_data(data):
    """Delete *data* from the database."""
    with get_engine().begin() as conn:
        d = delete(Data.T).where(Data.T.c.id == data.id)
        conn.execute(d)
