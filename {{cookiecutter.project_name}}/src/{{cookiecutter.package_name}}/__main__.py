import click


@click.command()
def main():
    """Console script for {{cookiecutter.project_name}}"""
    click.echo("Replace this message by putting your code into "
               "{{cookiecutter.project_name}}.__main__.main")
    click.echo("See click documentation at http://click.pocoo.org/")


if __name__ == "__main__":
    main()
